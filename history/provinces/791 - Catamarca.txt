# No previous file for Catamarca
owner = B43
controller = B43
add_core = B43
culture = common_goblin
religion = goblinic_shamanism


base_tax = 1
base_production = 1
base_manpower = 2

trade_goods = fur

capital = ""

is_city = yes

native_size = 36
native_ferocity = 5
native_hostileness = 10

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish
discovered_by = tech_harpy
discovered_by = tech_goblin
discovered_by = tech_bulwari