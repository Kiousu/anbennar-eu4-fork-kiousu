# No previous file for Estallen
owner = A93
controller = A93
add_core = A93
culture = esmari
religion = regent_court

hre = yes

base_tax = 9
base_production = 8
base_manpower = 7

trade_goods = cloth
center_of_trade = 1

capital = ""

is_city = yes
fort_15th = yes 

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish