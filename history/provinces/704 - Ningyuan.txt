# No previous file for Ningyuan
owner = Z31
controller = Z31
add_core = Z31
culture = blue_reachman
religion = regent_court

hre = no

base_tax = 4
base_production = 4
base_manpower = 2

trade_goods = naval_supplies

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_gnomish
discovered_by = tech_kobold