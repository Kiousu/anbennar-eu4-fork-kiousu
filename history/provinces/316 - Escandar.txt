# No previous file for Escandar
owner = A48
controller = A48
add_core = A48
culture = wexonard
religion = regent_court

hre = yes

base_tax = 3
base_production = 4
base_manpower = 3

trade_goods = iron

capital = ""

is_city = yes
fort_15th = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish
discovered_by = tech_harpy
discovered_by = tech_goblin
discovered_by = tech_bulwari

add_permanent_province_modifier = {
	name = calasandurs_elven_castle
	duration = -1
}

add_permanent_province_modifier = {
	name = gnollish_minority_coexisting_large
	duration = -1
}