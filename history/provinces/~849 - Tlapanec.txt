# No previous file for Tlapanec
owner = B17
controller = B17
add_core = B17
culture = silver_dwarf
religion = regent_court

hre = no

base_tax = 2
base_production = 2
base_manpower = 2

trade_goods = unknown

capital = ""

is_city = yes

native_size = 0
native_ferocity = 0
native_hostileness = 0