#525 - Gordihr

owner = F23
controller = F23
add_core = F23
culture = copper_dwarf
religion = ancestor_worship

base_tax = 8
base_production = 8
base_manpower = 5

trade_goods = naval_supplies

capital = ""

is_city = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish
discovered_by = tech_harpy
discovered_by = tech_goblin
discovered_by = tech_bulwari


add_permanent_province_modifier = {
	name = human_minority_coexisting_small
	duration = -1
}