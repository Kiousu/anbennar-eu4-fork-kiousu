#85 - Koln | Sornbay

owner = A09
controller = A09
add_core = A09
culture = sorncosti
religion = regent_court
hre = no
base_tax = 10
base_production = 10
trade_goods = wine
center_of_trade = 2
base_manpower = 6
capital = "City of the Western Wine"
is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish

add_permanent_province_modifier = {
	name = sornroy_estuary_modifier
	duration = -1
}

add_permanent_province_modifier = {
	name = elven_minority_coexisting_small
	duration = -1
}