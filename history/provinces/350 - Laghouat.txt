# No previous file for Laghouat

owner = A83
controller = A83
add_core = A83
culture = moon_elf
religion = elven_forebears

hre = no

base_tax = 3
base_production = 5
base_manpower = 2

trade_goods = incense

capital = ""

is_city = yes
fort_15th = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_orcish