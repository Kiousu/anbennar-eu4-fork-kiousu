#34 - Dalmatia | 

owner = A87
controller = A87
add_core = A87
add_core = A78
culture = roilsardi
religion = regent_court

hre = yes

base_tax = 4
base_production = 4
base_manpower = 4

trade_goods = cloth
capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish