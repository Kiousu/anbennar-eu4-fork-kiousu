#77 - Heidelberg | Gladegate

owner = A03
controller = A03
add_core = A03
culture = moon_elf
religion = regent_court

hre = no

base_tax = 5
base_production = 4
base_manpower = 3

trade_goods = wine

capital = "Entrance to the Redglades"

is_city = yes
fort_15th = yes 

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish

