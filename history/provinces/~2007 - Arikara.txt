# No previous file for Arikara
culture = selphereg
religion = eordellon
capital = "Arkeprun"

hre = no

base_tax = 4
base_production = 4
base_manpower = 3

trade_goods = incense

native_size = 14
native_ferocity = 6
native_hostileness = 6