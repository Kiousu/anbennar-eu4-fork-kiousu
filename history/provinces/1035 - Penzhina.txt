# No previous file for Penzhina
owner = G02
controller = G02
add_core = G02
culture = boek
religion = totemism
capital = ""

hre = no

base_tax = 3
base_production = 4
base_manpower = 2

trade_goods = unknown

native_size = 11
native_ferocity = 5
native_hostileness = 3