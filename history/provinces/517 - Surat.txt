#517 - Eloan

owner = F17
controller = F17
add_core = F17
culture = crathanori
religion = regent_court


base_tax = 3
base_production = 3
base_manpower = 3

trade_goods = fish

capital = ""

is_city = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish
discovered_by = tech_harpy
discovered_by = tech_goblin
discovered_by = tech_bulwari


add_permanent_province_modifier = {
	name = elven_minority_coexisting_small
	duration = -1
}
