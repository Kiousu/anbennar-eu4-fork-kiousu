government = monarchy
add_government_reform = autocracy_reform
government_rank = 3
primary_culture = castanorian
religion = regent_court
technology_group = tech_cannorian
capital = 840
national_focus = DIP

1000.1.1 = { set_country_flag = mage_organization_decentralized_flag }