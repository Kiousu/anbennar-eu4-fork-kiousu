government = theocracy
add_government_reform = leading_clergy_reform
government_rank = 1
primary_culture = olavish
religion = skaldhyrric_faith
technology_group = tech_cannorian
capital = 976
national_focus = DIP

1431.5.10 = {
	monarch = {
		name = "Leif of Nautakr"
		birth_date = 1384.10.18
		adm = 3
		dip = 2
		mil = 3
	}
}
1000.1.1 = { set_country_flag = mage_organization_decentralized_flag }