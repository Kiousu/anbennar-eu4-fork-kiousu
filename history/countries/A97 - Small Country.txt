government = republic
add_government_reform = oligarchy_reform
government_rank = 2
primary_culture = redfoot_halfling
religion = regent_court
technology_group = tech_cannorian
national_focus = DIP
capital = 159
add_accepted_culture = bluefoot_halfling
add_accepted_culture = imperial_halfling

1000.1.1 = { set_country_flag = mage_organization_decentralized_flag }