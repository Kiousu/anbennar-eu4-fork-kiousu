namespace = anb_miscevents

#CountyToDuchy - vassal plea
country_event = {
	id = anb_miscevents.1
	title = anb_miscevents.1.t
	desc = anb_miscevents.1.d
	picture = COURT_eventPicture

	is_triggered_only = yes

	
	option = {	#Let us hope they accept our rightful elevation
		name = anb_miscevents.1.a
		custom_tooltip = ct_anb_miscevents.1
		if = {
			limit = {
				is_subject = yes
			}
			overlord = { country_event = { id = anb_miscevents.2 days = 7 } }
		}
		if = {
			limit = {
				is_part_of_hre = yes
				is_subject = no
			}
			emperor = { country_event = { id = anb_miscevents.2 days = 7 } }
		}
	}
}

#CountyToDuchy - overlord response
country_event = {
	id = anb_miscevents.2
	title = anb_miscevents.2.t
	desc = anb_miscevents.2.d
	picture = COURT_eventPicture

	is_triggered_only = yes


	option = {	#Of course, they've proven their worth
		name = anb_miscevents.2.a
		ai_chance = { 
			factor = 20
			modifier = {
				factor = 3
				ruler_has_personality = benevolent_personality
			}
			modifier = {
				factor = 5
				has_opinion = {
					who = FROM
					value = 140
				}
			}
		}
		FROM = { add_opinion = { who = ROOT modifier = opinion_countytoduchy_accept } }
		custom_tooltip = ct_anb_miscevents.2a
		add_prestige = -10
		
		hidden_effect = {
			FROM = { country_event = { id = anb_miscevents.3 days = 7 } }
		}
		
	}
	option = {	#Oh I dont think so
		name = anb_miscevents.2.b
		ai_chance = { 
			factor = 80
			modifier = {
				factor = 10
				ruler_has_personality = malevolent_personality
			}
		}
		FROM = { add_opinion = { who = ROOT modifier = opinion_countytoduchy_reject } }
		custom_tooltip = ct_anb_miscevents.2b
		
		hidden_effect = {
			FROM = { country_event = { id = anb_miscevents.4 days = 7 } }
		}
	}
}

#CountyToDuchy - overlord says yes
country_event = {
	id = anb_miscevents.3
	title = anb_miscevents.3.t
	desc = anb_miscevents.3.d
	picture = COURT_eventPicture

	is_triggered_only = yes


	option = {	#All hail the new Duchy of [CountryName]
		name = anb_miscevents.3.a
		hidden_effect = {
			clr_country_flag = is_a_county
		}
		custom_tooltip = ct_anb_miscevents.3
		add_prestige = 70
		ROOT = { add_opinion = { who = FROM modifier = opinion_countytoduchy_accept } }
	}
}

#CountyToDuchy - overlord says no
country_event = {
	id = anb_miscevents.4
	title = anb_miscevents.4.t
	desc = anb_miscevents.4.d
	picture = COURT_eventPicture

	is_triggered_only = yes

	
	option = {	#Rejection?! First of all, how dare you...
		name = anb_miscevents.4.a
		custom_tooltip = ct_anb_miscevents.4
		add_prestige = -10
		#ROOT = { add_opinion = { who = FROM modifier = opinion_countytoduchy_reject } }
	}
}

#CountyToDuchy - independent automatically becomes duke
country_event = {
	id = anb_miscevents.5
	title = anb_miscevents.5.t
	desc = anb_miscevents.5.d
	picture = COURT_eventPicture

	is_triggered_only = yes

	
	option = {	#All hail the new Duchy of [CountryName]
		name = anb_miscevents.5.a
		hidden_effect = {
			clr_country_flag = is_a_county
		}
		custom_tooltip = ct_anb_miscevents.3
		add_prestige = 70
	}
}


#heirflag test
country_event = {
	id = anb_miscevents.6
	title = anb_miscevents.6.t
	desc = anb_miscevents.6.d
	picture = COURT_eventPicture

	is_triggered_only = yes

	
	option = {	#All hail the new Duchy of [CountryName]
		name = anb_miscevents.5.a
		set_heir_flag = heir_flag_test
	}
}