namespace = magic_realm_necromancy


#Necromancy - Menu
country_event = {
	id = magic_realm_necromancy.0
	title = magic_realm_necromancy.0.t
	desc = magic_realm_necromancy.0.d
	picture = BIG_BOOK_eventPicture
	
	is_triggered_only = yes
	
	option = {
		name = magic_realm_nospells.0.a
		trigger = {
			NOT = { has_ruler_flag = necromancy_1 }
			NOT = { has_ruler_flag = necromancy_2 }
			NOT = { has_ruler_flag = necromancy_3 }
		}
		ai_chance = {
			factor = 50
		}
		
		country_event = { id = magic_realm.0 days = 0 }
	}
	
	#Absorb life energy aka kill a random advisor, consort, heir and increase your stats
	option = {	# necromancy.1 - [1] Absorb Life Energy
		name = magic_realm_necromancy.1.t
		trigger = {
			has_ruler_flag = necromancy_1
		}
		ai_chance = {
			factor = 50
		}
		
		country_event = { id = magic_realm_necromancy.1 days = 0 }
		
		custom_tooltip = tooltip_option_necromancy_1
	}
	
	option = {	# necromancy.2 - [2] Summon Undead Army
		name = magic_realm_necromancy.2.t
		trigger = {
			has_ruler_flag = necromancy_2
		}
		ai_chance = {
			factor = 25
			modifier = {
				factor = 4
				is_at_war = yes
			}
			modifier = {
				factor = 0.5
				NOT = { has_ruler_modifier = witch_king_modifier }
			}
			modifier = {
				factor = 0
				OR = {
					ruler_has_personality = benevolent_personality
					ruler_has_personality = careful_personality
					ruler_has_personality = craven_personality
					ruler_has_personality = charismatic_negotiator_personality
				}
			}
		}
		
		country_event = { id = magic_realm_necromancy.2 days = 0 }
		
		custom_tooltip = tooltip_option_necromancy_2
	}
	
	option = {	# necromancy.3 - [3] Pursue Lichdom
		name = magic_realm_necromancy.3.t
		trigger = {
			has_ruler_flag = necromancy_3
		}
		ai_chance = {
			factor = 50
			modifier = {
				factor = 5
				ruler_age = 40
				culture_group_is_short_lived = yes
			}
			modifier = {
				factor = 0.25
				culture_group_is_short_lived = no
			}
			modifier = {
				factor = 0
				has_ruler_flag = magic_project_lichdom_started
			}
		}
		
		country_event = { id = magic_realm_necromancy.3 days = 0 }
		
		custom_tooltip = tooltip_option_necromancy_3
	}
	
	option = {
		name = magic_siege.goback
		ai_chance = {
			factor = 5
		}
		country_event = { id = magic_realm.0 days = 0 }
	}
}

# 1 - Absorb Life Energy
country_event = {
	id = magic_realm_necromancy.1
	title = magic_realm_necromancy.1.t
	desc = magic_realm_necromancy.1.d
	picture = NEW_HEIR_eventPicture
	
	is_triggered_only = yes
	
	option = {	#Absorb Heir
		name = magic_realm_necromancy.1.a
		trigger = {
			has_ruler_flag = necromancy_1
			has_heir = yes
		}
		ai_chance = {
			factor = 50
			modifier = {
				factor = 0.5
				government = monarchy
				OR = {
					heir_adm = 3
					heir_dip = 3
					heir_mil = 3
				}
			}
		}
		
		#Cost
		add_adm_power = -5
		add_mil_power = -5
		
		add_legitimacy = -10
		add_devotion = -10
		
		#Effect
		increase_witch_king_points_small = yes
		
		magic_casted_spell = yes
		
		random = {
			chance = 25
			kill_heir = yes
		}
	
		
		random_list = {
			33 = {
				add_adm_power = 30
			}
			33 = {
				add_dip_power = 30
			}
			33 = {
				add_mil_power = 30
			}
		}
	}
	
	option = {	#Absorb Consort
		name = magic_realm_necromancy.1.b
		trigger = {
			has_ruler_flag = necromancy_1
			has_consort = yes
		}
		ai_chance = {
			factor = 50
			modifier = {
				factor = 0.5
				OR = {
					consort_adm = 3
					consort_dip = 3
					consort_mil = 3
				}
			}
		}
		
		#Cost
		add_adm_power = -5
		add_mil_power = -5
		
		add_legitimacy = -5
		add_prestige = -2
		
		#Effect
		increase_witch_king_points_small = yes
		
		magic_casted_spell = yes
		
		random = {
			chance = 25
			remove_consort = yes
		}
		
		random_list = {
			33 = {
				add_adm_power = 20
			}
			33 = {
				add_dip_power = 20
			}
			33 = {
				add_mil_power = 20
			}
		}
	}
	
	option = {	#Absorb Advisor
		name = magic_realm_necromancy.1.c
		trigger = {
			has_ruler_flag = necromancy_1
			has_advisor = yes
		}
		ai_chance = {
			factor = 50
		}
		
		#Cost
		add_adm_power = -5
		add_mil_power = -5
		
		add_prestige = -5
		
		#Effect
		increase_witch_king_points_small = yes
		
		magic_casted_spell = yes
		
		random = {
			chance = 25
			kill_advisor = random
		}
		
		random_list = {
			33 = {
				add_adm_power = 20
			}
			33 = {
				add_dip_power = 20
			}
			33 = {
				add_mil_power = 20
			}
		}
	}
	
	option = {	#Absorb the Populace
		name = magic_realm_necromancy.1.e
		trigger = {
			has_ruler_flag = necromancy_1
		}
		ai_chance = {
			factor = 50
			modifier = {
				factor = 0.75
				NOT = { num_of_cities = 20 }
			}
		}
		
		#Cost
		add_adm_power = -5
		add_dip_power = -5
		
		#Effect
		increase_witch_king_points_medium = yes
		
		magic_casted_spell = yes
		
		random_owned_province = {
			if = {
				limit = { base_tax = 2 }
				add_base_tax = -1
			}
			else_if = {
				limit = { base_manpower = 2 }
				add_base_manpower = -1
			}
			else_if = {
				limit = { base_manpower = 2 }
				add_base_production = -1
			}
		}
		
		random_list = {
			33 = {
				add_adm_power = 100
			}
			33 = {
				add_dip_power = 100
			}
			33 = {
				add_mil_power = 100
			}
		}
	}
	
	option = {	#Option B: Go back
		name = magic_siege.goback
		ai_chance = {
			factor = 5
		}
		country_event = { id = magic_realm_necromancy.0 days = 0 }
	}
}

# 2 - Summon Undead Army
country_event = {
	id = magic_realm_necromancy.2
	title = magic_realm_necromancy.2.t
	desc = magic_realm_necromancy.2.d
	picture = FAMINE_eventPicture
	
	is_triggered_only = yes
	
	immediate = {
		hidden_effect = {
			random_owned_province = {
				limit = {
					is_in_capital_area = yes
				}
				save_event_target_as = undead_army_province
			}
			random_owned_province = {
				limit = {
					is_in_capital_area = yes
					OR = {
						has_estate = estate_mages
						has_estate = estate_acolytes
					}
				}
				save_event_target_as = undead_army_province
			}
			random_owned_province = {
				limit = {
					is_capital = yes
				}
				save_event_target_as = undead_army_province
			}
		}
	}
	
	option = {	
		name = magic_realm_necromancy.2.a
		trigger = {
			has_ruler_flag = necromancy_2
			
			adm_power = 15
			dip_power = 15
			mil_power = 15
		}
		ai_chance = {
			factor = 50
		}
		
		#Cost
		add_adm_power = -15
		add_dip_power = -15
		add_mil_power = -15
		
		#Effect
		magic_casted_spell = yes
		
		if = {
			limit = { 
				is_necromancy_2 = yes
			}
			event_target:undead_army_province = {
				infantry = ROOT
				infantry = ROOT
				infantry = ROOT
				infantry = ROOT
				infantry = ROOT
			}
		}
		else = {	#necro 3
			event_target:undead_army_province = {
				infantry = ROOT
				infantry = ROOT
				infantry = ROOT
				infantry = ROOT
				infantry = ROOT
				infantry = ROOT
				infantry = ROOT
				infantry = ROOT
				cavalry = ROOT
				cavalry = ROOT
			}
		}
		
		increase_witch_king_points_large = yes
		increase_witch_king_points_large = yes
		increase_witch_king_points_large = yes
		increase_witch_king_points_large = yes
		increase_witch_king_points_large = yes
		
		if = {
			limit = {
				has_country_modifier = undead_army
			}
		}
		else = {
			add_country_modifier = {
				name = undead_army
				duration = -1
			}
		}
		
		if = {
			limit = {
				NOT = { technology_group = tech_undead }
			}
			#change_technology_group = tech_undead
			change_unit_type = tech_undead
		}
	}
	
	option = {	#Option B: Go back
		name = magic_siege.goback
		ai_chance = {
			factor = 5
		}
		country_event = { id = magic_realm_necromancy.0 days = 0 }
	}
}

# 3 - Pursuit of Lichdom
country_event = {
	id = magic_realm_necromancy.3
	title = magic_realm_necromancy.3.t
	desc = magic_realm_necromancy.3.d
	picture = END_OF_TIME_eventPicture
	
	is_triggered_only = yes
	
	option = {
		name = magic_realm_unavailable.0.a
		trigger = {
			has_ruler_flag = is_a_lich
		}
		custom_tooltip = tooltip_magic_realm_unavailable_already_lich
	}
	
	option = {
		name = magic_realm_project_in_progress.0.a
		trigger = {
			has_ruler_flag = magic_project_lichdom_started
		}
		custom_tooltip = tooltip_magic_realm_unavailable_project_in_progress
	}
	
	option = {
		name = magic_realm_necromancy.3.cost
		trigger = {
			OR = {
				NOT = { adm_power = 300 }
				NOT = { dip_power = 300 }
				NOT = { mil_power = 300 }
			}
		}
		country_event = { id = magic_realm_necromancy.0 days = 0 }
	}
	
	option = {	#Pursue Lichdom 
		name = magic_realm_necromancy.3.a
		trigger = {
			has_ruler_flag = necromancy_3
			NOT = { has_ruler_flag = magic_project_lichdom_started }
			NOT = { has_ruler_flag = magic_project_lichdom_complete }
			
			adm_power = 300
			dip_power = 300
			mil_power = 300
		}
		ai_chance = {
			factor = 50
		}
		
		#Cost
		add_adm_power = -300
		add_dip_power = -300
		add_mil_power = -300
		
		#Effect
		magic_casted_spell = yes
		
		set_ruler_flag = magic_project_lichdom_started
		custom_tooltip = tooltip_magic_project_start
		
		increase_witch_king_points_large = yes
	}
	
	option = {	#Option B: Go back
		name = magic_siege.goback
		ai_chance = {
			factor = 5
		}
		country_event = { id = magic_realm_necromancy.0 days = 0 }
	}
}
