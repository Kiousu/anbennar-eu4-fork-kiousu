namespace = theocracy

country_event = {
	id = adventurer.1
	title = theocracy.1.t
	desc = theocracy.1.desc
	picture = RELIGION_eventPicture

	is_triggered_only = yes

	trigger = {
		has_reform = adventurer_reform
		NOT = { has_country_flag = in_adventurer_heir_selection }
		has_government_attribute = heir
	}

	immediate = {
		hidden_effect = {
			set_country_flag = in_adventurer_heir_selection
			
			clr_country_flag = adv_adventure_captain_flag
			clr_country_flag = adv_marcher_flag
			clr_country_flag = adv_pioneer_flag
			clr_country_flag = adv_fortune_seeker_flag
			clr_country_flag = adv_lilac_wars_veteran_flag
			clr_country_flag = adv_noble_flag
			clr_country_flag = adv_escanni_native_flag
			clr_country_flag = adv_mage_flag
			
		}
	}
	
	
	# A Skilled adventurer Captain (+adventuring efficiency)
	option = {
		name = adventurer.1.a
		set_country_flag = adv_adventure_captain_flag # USED IN DEVOTION.TXT
		define_heir = {
			age = 30
			hidden = yes
		}
		add_militarised_society = 10
		clr_country_flag = in_adventurer_heir_selection
		custom_tooltip = theocracy.1.tt
	}

	
	# A Powerful Marcher (+marcher)
	option = {
		name = adventurer.1.b	
		set_country_flag = adv_marcher_flag # USED IN DEVOTION.TXT
		define_heir = {
			age = 30
			hidden = yes
		}
		add_faction_influence = { faction = adv_marchers influence = 10 }
		clr_country_flag = in_adventurer_heir_selection
		custom_tooltip = theocracy.1.tt
	}
	
	# A Charismatic Pioneer (+pioneer) this one doesnt show
	option = {
		name = "adventurer.1.c"
		set_country_flag = adv_pioneer_flag # USED IN DEVOTION.TXT
		define_heir = {
			age = 30
			hidden = yes
		}
		add_faction_influence = { faction = adv_pioneers influence = 10 }
		clr_country_flag = in_adventurer_heir_selection
		custom_tooltip = theocracy.1.tt
	}
	
	# A Shrewd Fortune-Seeker (+fortune seeker) this one doesnt show
	option = {
		name = adventurer.1.dd
		set_country_flag = adv_fortune_seeker_flag # USED IN DEVOTION.TXT
		define_heir = {
			age = 30
			hidden = yes
		}
		add_faction_influence = { faction = adv_fortune_seekers influence = 10 }
		clr_country_flag = in_adventurer_heir_selection
		custom_tooltip = theocracy.1.tt
	}
	
	# A Lilac Wars Veteran (guaranteed high mil) this doesnt work
	option = {
		name = adventurer.1.e
		trigger = {
			NOT = { is_year = 1500 }
		}
		set_country_flag = adv_lilac_wars_veteran_flag # USED IN DEVOTION.TXT
		# random_country = {
			# limit = {
				# has_regency = no
				# religion = ROOT
				# culture_group = ROOT
				# NOT = { has_reform = adventurer_reform {
				# NOT = { continent = north_america }
				# NOT = { continent = south_america }
			# }
			# ROOT = {
				# define_heir = { 
					# dynasty = " " 
					# age = 30
					# culture = PREV
					# mil = 4
					# hidden = yes
				# }
			# }
		# }
		define_heir = {
			age = 30
			mil = 4
			hidden = yes
		}
		clr_country_flag = in_adventurer_heir_selection
		custom_tooltip = theocracy.1.tt
	}
	
	# A Noble from our Homeland (+gold)
	option = {
		name = adventurer.1.f
		trigger = {
			any_country = {
				limit = {
					government = monarchy
					religion = ROOT
					culture = ROOT
					NOT = { continent = north_america }
					NOT = { continent = south_america }
				}
			}
		}
		set_country_flag = adv_noble_flag # USED IN DEVOTION.TXT
		# random_known_country = {
			# limit = {
				# government = monarchy
				# religion = ROOT
				# culture = ROOT
				# NOT = { continent = north_america }
				# NOT = { continent = south_america }
			# }
			# ROOT = {
				# define_heir = { 
					# dynasty = PREV 
					# age = 30
					# culture = PREV
					# hidden = yes
				# }
			# }
			# add_opinion = { who = ROOT modifier = opinion_adventurer_noble }
		# }
		random_known_country = {
			limit = {
				government = monarchy
				has_regency = no
				religion = ROOT
				culture = ROOT
				NOT = { continent = north_america }
				NOT = { continent = south_america }	#this needs to be changed in the future to allow adventurers from all over, eg Halessi adventurers - how do we limit it to their homeland?
			}
			ROOT = { 
				define_heir = { 
					dynasty = PREV 
					age = 30
					culture = PREV
					hidden = yes
				}
			}
			add_opinion = { who = ROOT modifier = opinion_theocracy_noble }
		}
		add_prestige = 10
		clr_country_flag = in_adventurer_heir_selection
		custom_tooltip = theocracy.1.tt
	}
	
	
	# An Escanni Native
	option = {
		name = adventurer.1.g	
		trigger = {
			capital_scope = { superregion = escann_superregion }
		}
		set_country_flag = adv_escanni_native_flag # USED IN DEVOTION.TXT
		if = {
			limit = {
				capital_scope = {
					region = inner_castanor_region
				}
			}
			define_heir = {
				age = 30
				hidden = yes
				culture = castellyrian
			}
		}
		else_if = {
			limit = {
				capital_scope = {
					region = west_castanor_region
				}
			}
			define_heir = {
				age = 30
				hidden = yes
				culture = adenner
			}
		}
		else_if = {
			limit = {
				capital_scope = {
					region = south_castanor_region
				}
			}
			define_heir = {
				age = 30
				hidden = yes
				culture = marcher
			}
		}
		add_yearly_manpower = 1
		clr_country_flag = in_adventurer_heir_selection
		custom_tooltip = theocracy.1.tt
	}
	
	# A Mage
	option = {
		name = adventurer.1.h
		trigger = {
			OR = {
				tag = B20
				tag = B19
			}
		}
		set_country_flag = adv_mage_flag # USED IN DEVOTION.TXT
		
		if = {
			limit = {
				has_saved_event_target = mage_province
			}
			define_heir = {
				age = 30
				hidden = yes
				culture = event_target:mage_province
			}
		}
		else = {
			define_heir = {
				age = 30
				hidden = yes
			}
		}
		hidden_effect = { add_heir_personality = mage_personality }
		clr_country_flag = in_adventurer_heir_selection
		custom_tooltip = theocracy.1.tt
		
		
		#Something that can randomize
	}
	
}