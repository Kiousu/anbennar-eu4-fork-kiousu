namespace = magic_realm_enchantment


#Enchantment - Menu
country_event = {
	id = magic_realm_enchantment.0
	title = magic_realm_enchantment.0.t
	desc = magic_realm_enchantment.0.d
	picture = BIG_BOOK_eventPicture
	
	is_triggered_only = yes
	
	option = {
		name = magic_realm_nospells.0.a
		trigger = {
			NOT = { has_ruler_flag = enchantment_1 }
			NOT = { has_ruler_flag = enchantment_2 }
			NOT = { has_ruler_flag = enchantment_3 }
		}
		ai_chance = {
			factor = 50
		}
		
		country_event = { id = magic_realm.0 days = 0 }
	}
	
	option = {	#Charm 1
		name = magic_realm_enchantment.1.t
		trigger = {
			has_ruler_flag = enchantment_1
		}
		ai_chance = {
			factor = 50
			modifier = {
				factor = 2
				stability = 1
			}
			modifier = {
				factor = 2
				is_at_war = yes
			}
		}
		
		country_event = { id = magic_realm_enchantment.1 days = 0 }
		
		custom_tooltip = tooltip_option_enchantment_1
	}
	
	option = {	#Modify Memories 2
		name = magic_realm_enchantment.2.t
		trigger = {
			has_ruler_flag = enchantment_2
		}
		ai_chance = {
			factor = 50
			modifier = {
				factor = 5
				overextension_percentage = 0.5
			}
			modifier = {
				factor = 2
				average_unrest = 4
			}
		}
		
		country_event = { id = magic_realm_enchantment.2 days = 0 }
		
		custom_tooltip = tooltip_option_enchantment_2
	}
	
	# option = {	#Enthrall the Nation? 3
		# name = magic_realm_enchantment.3.t
		# trigger = {
			# has_ruler_flag = enchantment_3
		# }
		# ai_chance = {
			# factor = 50
		# }
		
		# country_event = { id = magic_realm_enchantment.3 days = 0 }
		
		# custom_tooltip = tooltip_option_enchantment_3
	# }
	
	option = {
		name = magic_siege.goback
		ai_chance = {
			factor = 50
		}
		country_event = { id = magic_realm.0 days = 0 }
	}
}

# 1 - Charm
country_event = {
	id = magic_realm_enchantment.1
	title = magic_realm_enchantment.1.t
	desc = magic_realm_enchantment.1.d
	picture = STREET_SPEECH_eventPicture
	
	is_triggered_only = yes
	
	option = {
		name = magic_realm_unavailable.0.a
		trigger = {
			AND = {
				OR = {
					has_ruler_modifier = magic_realm_enchantment_charm_impress_foreign_diplomats_1
					has_ruler_modifier = magic_realm_enchantment_charm_impress_foreign_diplomats_2
				}
				OR = {
					has_ruler_modifier = magic_realm_enchantment_charm_inspire_military_1
					has_ruler_modifier = magic_realm_enchantment_charm_inspire_military_2
				}
				OR = {
					has_ruler_modifier = magic_realm_enchantment_charm_assuage_subjects_1
					has_ruler_modifier = magic_realm_enchantment_charm_assuage_subjects_2
				}
			}
		}
		custom_tooltip = tooltip_magic_realm_unavailable_cooldown
	}
	
	option = {
		name = magic_realm_enchantment.1.cost
		trigger = {
			OR = {
				NOT = { dip_power = 30 }
				NOT = { mil_power = 10 }
			}
		}
		country_event = { id = magic_realm_enchantment.0 days = 0 }
	}
	
	option = {	#Sway the Estates
		name = magic_realm_enchantment.1.a
		trigger = {
			dip_power = 30
			mil_power = 10
		
		}
		ai_chance = {
			factor = 50
			modifier = {
				factor = 2
				OR = {
					estate_loyalty = {
						estate = estate_church
						loyalty = 30
					}
					estate_loyalty = {
						estate = estate_nobles
						loyalty = 30
					}
					estate_loyalty = {
						estate = estate_burghers
						loyalty = 30
					}
					estate_loyalty = {
						estate = estate_nomadic_tribes 
						loyalty = 30
					}
					estate_loyalty = {
						estate = estate_mages 
						loyalty = 30
					}
					estate_loyalty = {
						estate = estate_adventurers 
						loyalty = 30
					}
					estate_loyalty = {
						estate = estate_artificers 
						loyalty = 30
					}
				}
			}
		}
		
		#Cost
		add_mil_power = -10
		add_dip_power = -30
		
		#Effect
		magic_casted_spell = yes
		
		#Enchantment 1
		if = {
			limit = {
				has_estate = estate_church
				has_ruler_flag = enchantment_1
				NOT = { has_ruler_flag = enchantment_2 }
				NOT = { has_ruler_flag = enchantment_3 }
			}
			add_estate_loyalty = {
				estate = estate_church
				loyalty = 3
			}
		}
		if = {
			limit = {
				has_estate = estate_nobles 
				has_ruler_flag = enchantment_1
				NOT = { has_ruler_flag = enchantment_2 }
				NOT = { has_ruler_flag = enchantment_3 }
			}
			add_estate_loyalty = {
				estate = estate_nobles 
				loyalty = 3
			}
		}
		if = {
			limit = {
				has_estate = estate_burghers  
				has_ruler_flag = enchantment_1
				NOT = { has_ruler_flag = enchantment_2 }
				NOT = { has_ruler_flag = enchantment_3 }
			}
			add_estate_loyalty = {
				estate = estate_burghers  
				loyalty = 3
			}
		}
		if = {
			limit = {
				has_estate = estate_nomadic_tribes  
				has_ruler_flag = enchantment_1
				NOT = { has_ruler_flag = enchantment_2 }
				NOT = { has_ruler_flag = enchantment_3 }
			}
			add_estate_loyalty = {
				estate = estate_nomadic_tribes  
				loyalty = 3
			}
		}
		if = {
			limit = {
				has_estate = estate_mages   
				has_ruler_flag = enchantment_1
				NOT = { has_ruler_flag = enchantment_2 }
				NOT = { has_ruler_flag = enchantment_3 }
			}
			add_estate_loyalty = {
				estate = estate_mages  
				loyalty = 3
			}
		}
		if = {
			limit = {
				has_estate = estate_adventurers    
				has_ruler_flag = enchantment_1
				NOT = { has_ruler_flag = enchantment_2 }
				NOT = { has_ruler_flag = enchantment_3 }
			}
			add_estate_loyalty = {
				estate = estate_adventurers   
				loyalty = 3
			}
		}
		if = {
			limit = {
				has_estate = estate_artificers    
				has_ruler_flag = enchantment_1
				NOT = { has_ruler_flag = enchantment_2 }
				NOT = { has_ruler_flag = enchantment_3 }
			}
			add_estate_loyalty = {
				estate = estate_artificers   
				loyalty = 3
			}
		}
		
		#Enchantment 2
		if = {
			limit = {
				has_estate = estate_church
				has_ruler_flag = enchantment_2
				NOT = { has_ruler_flag = enchantment_3 }
			}
			add_estate_loyalty = {
				estate = estate_church
				loyalty = 7
			}
		}
		if = {
			limit = {
				has_estate = estate_nobles 
				has_ruler_flag = enchantment_2
				NOT = { has_ruler_flag = enchantment_3 }
			}
			add_estate_loyalty = {
				estate = estate_nobles 
				loyalty = 7
			}
		}
		if = {
			limit = {
				has_estate = estate_burghers  
				has_ruler_flag = enchantment_2
				NOT = { has_ruler_flag = enchantment_3 }
			}
			add_estate_loyalty = {
				estate = estate_burghers  
				loyalty = 7
			}
		}
		if = {
			limit = {
				has_estate = estate_nomadic_tribes  
				has_ruler_flag = enchantment_2
				NOT = { has_ruler_flag = enchantment_3 }
			}
			add_estate_loyalty = {
				estate = estate_nomadic_tribes  
				loyalty = 7
			}
		}
		if = {
			limit = {
				has_estate = estate_mages   
				has_ruler_flag = enchantment_2
				NOT = { has_ruler_flag = enchantment_3 }
			}
			add_estate_loyalty = {
				estate = estate_mages  
				loyalty = 7
			}
		}
		if = {
			limit = {
				has_estate = estate_adventurers    
				has_ruler_flag = enchantment_2
				NOT = { has_ruler_flag = enchantment_3 }
			}
			add_estate_loyalty = {
				estate = estate_adventurers   
				loyalty = 7
			}
		}
		if = {
			limit = {
				has_estate = estate_artificers    
				has_ruler_flag = enchantment_2
				NOT = { has_ruler_flag = enchantment_3 }
			}
			add_estate_loyalty = {
				estate = estate_artificers   
				loyalty = 7
			}
		}
		
		#Enchantment 3
		if = {
			limit = {
				has_estate = estate_church
				has_ruler_flag = enchantment_3
			}
			add_estate_loyalty = {
				estate = estate_church
				loyalty = 10
			}
		}
		if = {
			limit = {
				has_estate = estate_nobles 
				has_ruler_flag = enchantment_3
			}
			add_estate_loyalty = {
				estate = estate_nobles 
				loyalty = 10
			}
		}
		if = {
			limit = {
				has_estate = estate_burghers  
				has_ruler_flag = enchantment_3
			}
			add_estate_loyalty = {
				estate = estate_burghers  
				loyalty = 10
			}
		}
		if = {
			limit = {
				has_estate = estate_nomadic_tribes  
				has_ruler_flag = enchantment_3
			}
			add_estate_loyalty = {
				estate = estate_nomadic_tribes  
				loyalty = 10
			}
		}
		if = {
			limit = {
				has_estate = estate_mages   
				has_ruler_flag = enchantment_3
			}
			add_estate_loyalty = {
				estate = estate_mages  
				loyalty = 10
			}
		}
		if = {
			limit = {
				has_estate = estate_adventurers    
				has_ruler_flag = enchantment_3
			}
			add_estate_loyalty = {
				estate = estate_adventurers   
				loyalty = 10
			}
		}
		if = {
			limit = {
				has_estate = estate_artificers    
				has_ruler_flag = enchantment_3
			}
			add_estate_loyalty = {
				estate = estate_artificers   
				loyalty = 10
			}
		}
		
	}
	
	option = {	#Impress Foreign Diplomats
		name = magic_realm_enchantment.1.b
		trigger = {
			OR = {
				NOT = { has_ruler_modifier = magic_realm_enchantment_charm_impress_foreign_diplomats_1 }
				NOT = { has_ruler_modifier = magic_realm_enchantment_charm_impress_foreign_diplomats_2 }
			}
			
			dip_power = 30
			mil_power = 10
		}
		ai_chance = {
			factor = 50
			modifier = {
				factor = 2
				personality = ai_diplomat
			}
		}
		
		#Cost
		add_mil_power = -10
		add_dip_power = -30
		
		#Effect
		magic_casted_spell = yes
		
		if = {
			limit = {
				OR = {
					AND = {
						dip = 0
						NOT = { dip = 2 }
						has_ruler_flag = enchantment_1
					}
				}
			}
			random_list = {
				90 = {
					add_ruler_modifier = {
						name = magic_realm_enchantment_charm_impress_foreign_diplomats_1
						duration = 365
					}
				}
				10 = {
					add_ruler_modifier = {
						name = magic_realm_enchantment_charm_impress_foreign_diplomats_2
						duration = 365
					}
				}
			}
		}
		else_if = {
			limit = {
				OR = {
					AND = {
						dip = 2
						NOT = { dip = 4 }
						has_ruler_flag = enchantment_1
					}
					AND = {
						dip = 0
						NOT = { dip = 2 }
						has_ruler_flag = enchantment_2
					}
				}
			}
			random_list = {
				80 = {
					add_ruler_modifier = {
						name = magic_realm_enchantment_charm_impress_foreign_diplomats_1
						duration = 365
					}
				}
				20 = {
					add_ruler_modifier = {
						name = magic_realm_enchantment_charm_impress_foreign_diplomats_2
						duration = 365
					}
				}
			}
		}
		else_if = {
			limit = {
				OR = {
					AND = {
						dip = 4
						NOT = { dip = 6 }
						has_ruler_flag = enchantment_1
					}
					AND = {
						dip = 2
						NOT = { dip = 4 }
						has_ruler_flag = enchantment_2
					}
					AND = {
						dip = 0
						NOT = { dip = 2 }
						has_ruler_flag = enchantment_3
					}
				}
			}
			random_list = {
				70 = {
					add_ruler_modifier = {
						name = magic_realm_enchantment_charm_impress_foreign_diplomats_1
						duration = 365
					}
				}
				30 = {
					add_ruler_modifier = {
						name = magic_realm_enchantment_charm_impress_foreign_diplomats_2
						duration = 365
					}
				}
			}
		}
		else_if = {
			limit = {
				OR = {
					AND = {
						dip = 6
						has_ruler_flag = enchantment_1
					}
					AND = {
						dip = 4
						NOT = { dip = 6 }
						has_ruler_flag = enchantment_2
					}
					AND = {
						dip = 2
						NOT = { dip = 4 }
						has_ruler_flag = enchantment_3
					}
				}
			}
			random_list = {
				60 = {
					add_ruler_modifier = {
						name = magic_realm_enchantment_charm_impress_foreign_diplomats_1
						duration = 365
					}
				}
				40 = {
					add_ruler_modifier = {
						name = magic_realm_enchantment_charm_impress_foreign_diplomats_2
						duration = 365
					}
				}
			}
		}
		else_if = {
			limit = {
				OR = {
					AND = {
						dip = 6
						has_ruler_flag = enchantment_2
					}
					AND = {
						dip = 4
						NOT = { dip = 6 }
						has_ruler_flag = enchantment_3
					}
				}
			}
			random_list = {
				50 = {
					add_ruler_modifier = {
						name = magic_realm_enchantment_charm_impress_foreign_diplomats_1
						duration = 365
					}
				}
				50 = {
					add_ruler_modifier = {
						name = magic_realm_enchantment_charm_impress_foreign_diplomats_2
						duration = 365
					}
				}
			}
		}
		else_if = {
			limit = {
				OR = {
					AND = {
						dip = 6
						has_ruler_flag = enchantment_3
					}
				}
			}
			random_list = {
				40 = {
					add_ruler_modifier = {
						name = magic_realm_enchantment_charm_impress_foreign_diplomats_1
						duration = 365
					}
				}
				60 = {
					add_ruler_modifier = {
						name = magic_realm_enchantment_charm_impress_foreign_diplomats_2
						duration = 365
					}
				}
			}
		}
		
	}
	
	option = {	#Inspire the Military
		name = magic_realm_enchantment.1.c
		trigger = {
			OR = {
				NOT = { has_ruler_modifier = magic_realm_enchantment_charm_inspire_military_1 }
				NOT = { has_ruler_modifier = magic_realm_enchantment_charm_inspire_military_2 }
			}
			
			dip_power = 30
			mil_power = 10
		}
		ai_chance = {
			factor = 50
			modifier = {
				factor = 2
				personality = ai_militarist
			}
			modifier = {
				factor = 3
				is_at_war = yes
			}
		}
		
		#Cost
		add_mil_power = -10
		add_dip_power = -30
		
		#Effect
		magic_casted_spell = yes
		
		if = {
			limit = {
				OR = {
					AND = {
						dip = 0
						NOT = { dip = 2 }
						has_ruler_flag = enchantment_1
					}
				}
			}
			random_list = {
				90 = {
					add_ruler_modifier = {
						name = magic_realm_enchantment_charm_inspire_military_1
						duration = 365
					}
				}
				10 = {
					add_ruler_modifier = {
						name = magic_realm_enchantment_charm_inspire_military_2
						duration = 365
					}
				}
			}
		}
		else_if = {
			limit = {
				OR = {
					AND = {
						dip = 2
						NOT = { dip = 4 }
						has_ruler_flag = enchantment_1
					}
					AND = {
						dip = 0
						NOT = { dip = 2 }
						has_ruler_flag = enchantment_2
					}
				}
			}
			random_list = {
				80 = {
					add_ruler_modifier = {
						name = magic_realm_enchantment_charm_inspire_military_1
						duration = 365
					}
				}
				20 = {
					add_ruler_modifier = {
						name = magic_realm_enchantment_charm_inspire_military_2
						duration = 365
					}
				}
			}
		}
		else_if = {
			limit = {
				OR = {
					AND = {
						dip = 4
						NOT = { dip = 6 }
						has_ruler_flag = enchantment_1
					}
					AND = {
						dip = 2
						NOT = { dip = 4 }
						has_ruler_flag = enchantment_2
					}
					AND = {
						dip = 0
						NOT = { dip = 2 }
						has_ruler_flag = enchantment_3
					}
				}
			}
			random_list = {
				70 = {
					add_ruler_modifier = {
						name = magic_realm_enchantment_charm_inspire_military_1
						duration = 365
					}
				}
				30 = {
					add_ruler_modifier = {
						name = magic_realm_enchantment_charm_inspire_military_2
						duration = 365
					}
				}
			}
		}
		else_if = {
			limit = {
				OR = {
					AND = {
						dip = 6
						has_ruler_flag = enchantment_1
					}
					AND = {
						dip = 4
						NOT = { dip = 6 }
						has_ruler_flag = enchantment_2
					}
					AND = {
						dip = 2
						NOT = { dip = 4 }
						has_ruler_flag = enchantment_3
					}
				}
			}
			random_list = {
				60 = {
					add_ruler_modifier = {
						name = magic_realm_enchantment_charm_inspire_military_1
						duration = 365
					}
				}
				40 = {
					add_ruler_modifier = {
						name = magic_realm_enchantment_charm_inspire_military_2
						duration = 365
					}
				}
			}
		}
		else_if = {
			limit = {
				OR = {
					AND = {
						dip = 6
						has_ruler_flag = enchantment_2
					}
					AND = {
						dip = 4
						NOT = { dip = 6 }
						has_ruler_flag = enchantment_3
					}
				}
			}
			random_list = {
				50 = {
					add_ruler_modifier = {
						name = magic_realm_enchantment_charm_inspire_military_1
						duration = 365
					}
				}
				50 = {
					add_ruler_modifier = {
						name = magic_realm_enchantment_charm_inspire_military_2
						duration = 365
					}
				}
			}
		}
		else_if = {
			limit = {
				OR = {
					AND = {
						dip = 6
						has_ruler_flag = enchantment_3
					}
				}
			}
			random_list = {
				40 = {
					add_ruler_modifier = {
						name = magic_realm_enchantment_charm_inspire_military_1
						duration = 365
					}
				}
				60 = {
					add_ruler_modifier = {
						name = magic_realm_enchantment_charm_inspire_military_2
						duration = 365
					}
				}
			}
		}
		
	}
	
	option = {	#Assuage Subjects
		name = magic_realm_enchantment.1.dd
		trigger = {
			OR = {
				NOT = { has_ruler_modifier = magic_realm_enchantment_charm_assuage_subjects_1 }
				NOT = { has_ruler_modifier = magic_realm_enchantment_charm_assuage_subjects_2 }
			}
			
			dip_power = 30
			mil_power = 10
		}
		ai_chance = {
			factor = 50
			modifier = {
				factor = 2
				num_of_subjects = 1
			}
			modifier = {
				factor = 5
				any_subject_country = { 
					liberty_desire = 50
				}
			}
		}
		
		#Cost
		add_mil_power = -10
		add_dip_power = -30
		
		#Effect
		magic_casted_spell = yes
		
		if = {
			limit = {
				OR = {
					AND = {
						dip = 0
						NOT = { dip = 2 }
						has_ruler_flag = enchantment_1
					}
				}
			}
			random_list = {
				90 = {
					add_ruler_modifier = {
						name = magic_realm_enchantment_charm_assuage_subjects_1
						duration = 365
					}
				}
				10 = {
					add_ruler_modifier = {
						name = magic_realm_enchantment_charm_assuage_subjects_2
						duration = 365
					}
				}
			}
		}
		else_if = {
			limit = {
				OR = {
					AND = {
						dip = 2
						NOT = { dip = 4 }
						has_ruler_flag = enchantment_1
					}
					AND = {
						dip = 0
						NOT = { dip = 2 }
						has_ruler_flag = enchantment_2
					}
				}
			}
			random_list = {
				80 = {
					add_ruler_modifier = {
						name = magic_realm_enchantment_charm_assuage_subjects_1
						duration = 365
					}
				}
				20 = {
					add_ruler_modifier = {
						name = magic_realm_enchantment_charm_assuage_subjects_2
						duration = 365
					}
				}
			}
		}
		else_if = {
			limit = {
				OR = {
					AND = {
						dip = 4
						NOT = { dip = 6 }
						has_ruler_flag = enchantment_1
					}
					AND = {
						dip = 2
						NOT = { dip = 4 }
						has_ruler_flag = enchantment_2
					}
					AND = {
						dip = 0
						NOT = { dip = 2 }
						has_ruler_flag = enchantment_3
					}
				}
			}
			random_list = {
				70 = {
					add_ruler_modifier = {
						name = magic_realm_enchantment_charm_assuage_subjects_1
						duration = 365
					}
				}
				30 = {
					add_ruler_modifier = {
						name = magic_realm_enchantment_charm_assuage_subjects_2
						duration = 365
					}
				}
			}
		}
		else_if = {
			limit = {
				OR = {
					AND = {
						dip = 6
						has_ruler_flag = enchantment_1
					}
					AND = {
						dip = 4
						NOT = { dip = 6 }
						has_ruler_flag = enchantment_2
					}
					AND = {
						dip = 2
						NOT = { dip = 4 }
						has_ruler_flag = enchantment_3
					}
				}
			}
			random_list = {
				60 = {
					add_ruler_modifier = {
						name = magic_realm_enchantment_charm_assuage_subjects_1
						duration = 365
					}
				}
				40 = {
					add_ruler_modifier = {
						name = magic_realm_enchantment_charm_assuage_subjects_2
						duration = 365
					}
				}
			}
		}
		else_if = {
			limit = {
				OR = {
					AND = {
						dip = 6
						has_ruler_flag = enchantment_2
					}
					AND = {
						dip = 4
						NOT = { dip = 6 }
						has_ruler_flag = enchantment_3
					}
				}
			}
			random_list = {
				50 = {
					add_ruler_modifier = {
						name = magic_realm_enchantment_charm_assuage_subjects_1
						duration = 365
					}
				}
				50 = {
					add_ruler_modifier = {
						name = magic_realm_enchantment_charm_assuage_subjects_2
						duration = 365
					}
				}
			}
		}
		else_if = {
			limit = {
				OR = {
					AND = {
						dip = 6
						has_ruler_flag = enchantment_3
					}
				}
			}
			random_list = {
				40 = {
					add_ruler_modifier = {
						name = magic_realm_enchantment_charm_assuage_subjects_1
						duration = 365
					}
				}
				60 = {
					add_ruler_modifier = {
						name = magic_realm_enchantment_charm_assuage_subjects_2
						duration = 365
					}
				}
			}
		}
		
	}
	
	option = {	#Option B: Go back
		name = magic_siege.goback
		ai_chance = {
			factor = 5
		}
		country_event = { id = magic_realm_enchantment.0 days = 0 }
	}
}

# 2 - Modify Memories
country_event = {
	id = magic_realm_enchantment.2
	title = magic_realm_enchantment.2.t
	desc = magic_realm_enchantment.2.d
	picture = CULTURE_eventPicture
	
	is_triggered_only = yes
	
	option = {
		name = magic_realm_unavailable.0.a
		trigger = {
			AND = {
				has_ruler_modifier = magic_realm_enchantment_modify_memories_overlook_national_blunders
				has_ruler_modifier = magic_realm_enchantment_modify_memories_encourage_subservience
				has_ruler_modifier = magic_realm_enchantment_modify_memories_forget_atrocities
			}
		}
		custom_tooltip = tooltip_magic_realm_unavailable_cooldown
	}
	
	option = {
		name = magic_realm_enchantment.2.cost
		trigger = {
			OR = {
				NOT = { adm_power = 150 }
				NOT = { mil_power = 80 }
			}
		}
		country_event = { id = magic_realm_enchantment.0 days = 0 }
	}
	
	
	option = {	#Overlook National Blunders
		name = magic_realm_enchantment.2.a
		trigger = {
			OR = {
				NOT = { has_ruler_modifier = magic_realm_enchantment_modify_memories_overlook_national_blunders }
			}
			
			adm_power = 150
			mil_power = 80
		}
		ai_chance = {
			factor = 50
			modifier = {
				factor = 5
				prestige = 1
				NOT = { prestige = 50 }
				
			}
		}
		
		#Cost
		add_mil_power = -80
		add_adm_power = -150
		
		#Effect
		magic_casted_spell = yes
		
		if = {
			limit = {
				OR = {
					AND = {
						dip = 0
						NOT = { dip = 2 }
						has_ruler_flag = enchantment_2
					}
				}
			}
			random_list = {
				90 = {
					add_ruler_modifier = {
						name = magic_realm_enchantment_modify_memories_overlook_national_blunders
						duration = 1825
					}
				}
				10 = {
					add_ruler_modifier = {
						name = magic_realm_enchantment_modify_memories_overlook_national_blunders
						duration = 3650
					}
				}
			}
		}
		else_if = {
			limit = {
				OR = {
					AND = {
						dip = 2
						NOT = { dip = 4 }
						has_ruler_flag = enchantment_2
					}
					AND = {
						dip = 0
						NOT = { dip = 2 }
						has_ruler_flag = enchantment_3
					}
				}
			}
			random_list = {
				80 = {
					add_ruler_modifier = {
						name = magic_realm_enchantment_modify_memories_overlook_national_blunders
						duration = 1825
					}
				}
				20 = {
					add_ruler_modifier = {
						name = magic_realm_enchantment_modify_memories_overlook_national_blunders
						duration = 3650
					}
				}
			}
		}
		else_if = {
			limit = {
				OR = {
					AND = {
						dip = 4
						NOT = { dip = 6 }
						has_ruler_flag = enchantment_2
					}
					AND = {
						dip = 2
						NOT = { dip = 4 }
						has_ruler_flag = enchantment_3
					}
				}
			}
			random_list = {
				70 = {
					add_ruler_modifier = {
						name = magic_realm_enchantment_modify_memories_overlook_national_blunders
						duration = 1825
					}
				}
				30 = {
					add_ruler_modifier = {
						name = magic_realm_enchantment_modify_memories_overlook_national_blunders
						duration = 3650
					}
				}
			}
		}
		else_if = {
			limit = {
				OR = {
					AND = {
						dip = 6
						has_ruler_flag = enchantment_2
					}
					AND = {
						dip = 4
						NOT = { dip = 6 }
						has_ruler_flag = enchantment_3
					}
				}
			}
			random_list = {
				60 = {
					add_ruler_modifier = {
						name = magic_realm_enchantment_modify_memories_overlook_national_blunders
						duration = 1825
					}
				}
				40 = {
					add_ruler_modifier = {
						name = magic_realm_enchantment_modify_memories_overlook_national_blunders
						duration = 3650
					}
				}
			}
		}
		else_if = {
			limit = {
				OR = {
					AND = {
						dip = 6
						has_ruler_flag = enchantment_3
					}
				}
			}
			random_list = {
				50 = {
					add_ruler_modifier = {
						name = magic_realm_enchantment_modify_memories_overlook_national_blunders
						duration = 1825
					}
				}
				50 = {
					add_ruler_modifier = {
						name = magic_realm_enchantment_modify_memories_overlook_national_blunders
						duration = 3650
					}
				}
			}
		}
		
	}
	
	option = {	#Encourage Subervience
		name = magic_realm_enchantment.2.b
		trigger = {
			OR = {
				NOT = { has_ruler_modifier = magic_realm_enchantment_modify_memories_encourage_subservience }
			}
			
			adm_power = 150
			mil_power = 80
		}
		ai_chance = {
			factor = 50
			modifier = {
				factor = 5
				average_autonomy_above_min = 20
			}
		}
		
		#Cost
		add_mil_power = -80
		add_adm_power = -150
		
		#Effect
		magic_casted_spell = yes
		
		if = {
			limit = {
				OR = {
					AND = {
						dip = 0
						NOT = { dip = 2 }
						has_ruler_flag = enchantment_2
					}
				}
			}
			random_list = {
				90 = {
					add_ruler_modifier = {
						name = magic_realm_enchantment_modify_memories_encourage_subservience
						duration = 1825
					}
				}
				10 = {
					add_ruler_modifier = {
						name = magic_realm_enchantment_modify_memories_encourage_subservience
						duration = 3650
					}
				}
			}
		}
		else_if = {
			limit = {
				OR = {
					AND = {
						dip = 2
						NOT = { dip = 4 }
						has_ruler_flag = enchantment_2
					}
					AND = {
						dip = 0
						NOT = { dip = 2 }
						has_ruler_flag = enchantment_3
					}
				}
			}
			random_list = {
				80 = {
					add_ruler_modifier = {
						name = magic_realm_enchantment_modify_memories_encourage_subservience
						duration = 1825
					}
				}
				20 = {
					add_ruler_modifier = {
						name = magic_realm_enchantment_modify_memories_encourage_subservience
						duration = 3650
					}
				}
			}
		}
		else_if = {
			limit = {
				OR = {
					AND = {
						dip = 4
						NOT = { dip = 6 }
						has_ruler_flag = enchantment_2
					}
					AND = {
						dip = 2
						NOT = { dip = 4 }
						has_ruler_flag = enchantment_3
					}
				}
			}
			random_list = {
				70 = {
					add_ruler_modifier = {
						name = magic_realm_enchantment_modify_memories_encourage_subservience
						duration = 1825
					}
				}
				30 = {
					add_ruler_modifier = {
						name = magic_realm_enchantment_modify_memories_encourage_subservience
						duration = 3650
					}
				}
			}
		}
		else_if = {
			limit = {
				OR = {
					AND = {
						dip = 6
						has_ruler_flag = enchantment_2
					}
					AND = {
						dip = 4
						NOT = { dip = 6 }
						has_ruler_flag = enchantment_3
					}
				}
			}
			random_list = {
				60 = {
					add_ruler_modifier = {
						name = magic_realm_enchantment_modify_memories_encourage_subservience
						duration = 1825
					}
				}
				40 = {
					add_ruler_modifier = {
						name = magic_realm_enchantment_modify_memories_encourage_subservience
						duration = 3650
					}
				}
			}
		}
		else_if = {
			limit = {
				OR = {
					AND = {
						dip = 6
						has_ruler_flag = enchantment_3
					}
				}
			}
			random_list = {
				50 = {
					add_ruler_modifier = {
						name = magic_realm_enchantment_modify_memories_encourage_subservience
						duration = 1825
					}
				}
				50 = {
					add_ruler_modifier = {
						name = magic_realm_enchantment_modify_memories_encourage_subservience
						duration = 3650
					}
				}
			}
		}
	}
	
	option = {	#Forget Atrocities
		name = magic_realm_enchantment.2.c
		trigger = {
			OR = {
				NOT = { has_ruler_modifier = magic_realm_enchantment_modify_memories_forget_atrocities }
			}
			
			adm_power = 150
			mil_power = 80
		}
		ai_chance = {
			factor = 50
			modifier = {
				factor = 2
				num_of_revolts = 1
			}
			modifier = {
				factor = 2
				average_unrest = 2
			}
		}
		
		#Cost
		add_mil_power = -80
		add_adm_power = -150
		
		#Effect
		magic_casted_spell = yes

		if = {
			limit = {
				OR = {
					AND = {
						dip = 0
						NOT = { dip = 2 }
						has_ruler_flag = enchantment_2
					}
				}
			}
			random_list = {
				90 = {
					add_ruler_modifier = {
						name = magic_realm_enchantment_modify_memories_forget_atrocities
						duration = 1825
					}
				}
				10 = {
					add_ruler_modifier = {
						name = magic_realm_enchantment_modify_memories_forget_atrocities
						duration = 3650
					}
				}
			}
		}
		else_if = {
			limit = {
				OR = {
					AND = {
						dip = 2
						NOT = { dip = 4 }
						has_ruler_flag = enchantment_2
					}
					AND = {
						dip = 0
						NOT = { dip = 2 }
						has_ruler_flag = enchantment_3
					}
				}
			}
			random_list = {
				80 = {
					add_ruler_modifier = {
						name = magic_realm_enchantment_modify_memories_forget_atrocities
						duration = 1825
					}
				}
				20 = {
					add_ruler_modifier = {
						name = magic_realm_enchantment_modify_memories_forget_atrocities
						duration = 3650
					}
				}
			}
		}
		else_if = {
			limit = {
				OR = {
					AND = {
						dip = 4
						NOT = { dip = 6 }
						has_ruler_flag = enchantment_2
					}
					AND = {
						dip = 2
						NOT = { dip = 4 }
						has_ruler_flag = enchantment_3
					}
				}
			}
			random_list = {
				70 = {
					add_ruler_modifier = {
						name = magic_realm_enchantment_modify_memories_forget_atrocities
						duration = 1825
					}
				}
				30 = {
					add_ruler_modifier = {
						name = magic_realm_enchantment_modify_memories_forget_atrocities
						duration = 3650
					}
				}
			}
		}
		else_if = {
			limit = {
				OR = {
					AND = {
						dip = 6
						has_ruler_flag = enchantment_2
					}
					AND = {
						dip = 4
						NOT = { dip = 6 }
						has_ruler_flag = enchantment_3
					}
				}
			}
			random_list = {
				60 = {
					add_ruler_modifier = {
						name = magic_realm_enchantment_modify_memories_forget_atrocities
						duration = 1825
					}
				}
				40 = {
					add_ruler_modifier = {
						name = magic_realm_enchantment_modify_memories_forget_atrocities
						duration = 3650
					}
				}
			}
		}
		else_if = {
			limit = {
				OR = {
					AND = {
						dip = 6
						has_ruler_flag = enchantment_3
					}
				}
			}
			random_list = {
				50 = {
					add_ruler_modifier = {
						name = magic_realm_enchantment_modify_memories_forget_atrocities
						duration = 1825
					}
				}
				50 = {
					add_ruler_modifier = {
						name = magic_realm_enchantment_modify_memories_forget_atrocities
						duration = 3650
					}
				}
			}
		}
		
	}
	
	option = {	#Option B: Go back
		name = magic_siege.goback
		ai_chance = {
			factor = 5
		}
		country_event = { id = magic_realm_enchantment.0 days = 0 }
	}
}