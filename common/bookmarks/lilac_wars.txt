bookmark =
{
	name = "LILACWARS_NAME"
	desc = "LILACWARS_DESC"
	date = 1444.11.11
	
	center = 8
	default = yes
	
	country = A01
	country = A02
	country = A04
	country = A11
	country = A12
	country = A13
	country = A46
	country = A25
	country = A45
	country = A30
	country = A33
	country = A29

	easy_country = A13
	easy_country = A11
	easy_country = A33
	easy_country = A29

}