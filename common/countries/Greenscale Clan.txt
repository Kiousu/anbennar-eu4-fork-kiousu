#Country Name: Please see filename.

graphical_culture = westerngfx

color = { 129  151  94 }

revolutionary_colors = { 129  151  94 }

historical_idea_groups = {
	exploration_ideas
	maritime_ideas
	economic_ideas
	offensive_ideas
	expansion_ideas
	quality_ideas	
	administrative_ideas	
	influence_ideas
}

historical_units = {
	kobold_1_clansmen
	kobold_5_skirmishers
	kobold_6_bigscale_rider
	kobold_9_pike_runners
	kobold_10_mounted_skirmishers
	kobold_12_sappers
	kobold_15_reformed_pike_runners
	kobold_17_reformed_bigscales
	kobold_19_dragonbreath_pistoliers
	kobold_23_wyrmfire_dreadnoughts
	kobold_26_reforged_bigscales
	kobold_26_reformed_pistoliers
}

monarch_names = {
	
	#Generic Kobold Name List
	
	"Arru #0" = 1
	"Deekin #0" = 1
	"Vrik #0" = 1
	"Nark #0" = 1
	"Drighad #0" = 1
	"Grator #0" = 1
	"Zurzu #0" = 1
	"Zurzumex #0" = 1
	"Nalnaz #0" = 1
	"Vud #0" = 1
	"Dak #0" = 1
	"Nak #0" = 1
	"Irto #0" = 1
	"Ogra #0" = 1
	"Kovu #0" = 1
	"Egi #0" = 1
	"Miglo #0" = 1
	"Vitru #0" = 1
	"Ekbo #0" = 1
	"Marku #0" = 1
	"Zold #0" = 1
	"Savlet #0" = 1
	"Mavlik #0" = 1
	"Sozrir #0" = 1
	"Znorbriz #0" = 1
	"Ravrurt #0" = 1
	"Erkil #0" = 1
	"Grigze #0" = 1
	"Qupzax #0" = 1
	"Dhan #0" = 1
	"Mork #0" = 1
	"Dizzap #0" = 1
	"Dhesesk #0" = 1
	"Realp #0" = 1
	"Dhild #0" = 1
	"Vhitir #0" = 1
	"Itrol #0" = 1
	"Zipzek #0" = 1
	"Qradez #0" = 1
	"Qalk #0" = 1
	"Aakax #0" = 1
	"Dheep #0" = 1
	"Rax #0" = 1
	"Nerlisk #0" = 1
	"Zarril #0" = 1
	"Zux #0" = 1
	"Adrax #0" = 1
	"Jukop #0" = 1
	"Vren #0" = 1
	"Qirnux #0" = 1

	"Alra #0" = -10
	"Bokidre #0" = -10
	"Dedro #0" = -10
	"Eru #0" = -10
	"Ete #0" = -10
	"Fegru #0" = -10
	"Feza #0" = -10
	"Gakar #0" = -10
	"Gazna #0" = -10
	"Geski #0" = -10
	"Gigla #0" = -10
	"Goksi #0" = -10
	"Hakrea #0" = -10
	"Ikse #0" = -10
	"Irpa #0" = -10
	"Ithroka #0" = -10
	"Itla #0" = -10
	"Kappa #0" = -10
	"Laadaa #0" = -10
	"Lerlam #0" = -10
	"Liznu #0" = -10
	"Lodka #0" = -10
	"Niksa #0" = -10
	"Odaka #0" = -10
	"Pirkin #0" = -10
	"Ris #0" = -10
	"Risra #0" = -10
	"Rudri #0" = -10
	"Saazeksa #0" = -10
	"Sahse #0" = -10
	"Sakkra #0" = -10
	"Sersa #0" = -10
	"Snepi #0" = -10
	"Tarikha #0" = -10
	"Tegil #0" = -10
	"Tibu #0" = -10
	"Tirba #0" = -10
	"Toskredka #0" = -10
	"Uho #0" = -10
	"Vakkra #0" = -10
	"Vezi #0" = -10
	"Visgri #0" = -10
	"Zazka #0" = -10
	"Zezi #0" = -10
	"Zolde #0" = -10
}

leader_names = {
	Scalesinger Scalemaster Greatfang Sneakscale Dragontail Sharpjaw Trapsetter Trapkin Spikemaker Scaleburrow Spiketail Traptail
	Lowear Hardwax Broadnose Smallbristle Talltooth Goodflame Losteye Smallpick Smallspike Smalltail Lazyteeth Trapsmith Trapmaster
	Guttersneak Burrowsneaker Trapsneak Grimeguard Bonebrand Bronzedelver Minedelver Minespike Minecrest Emberclaw
	Nimbleclaw Trapdagger Rumbledagger Minedagger Netherdagger Darktrap Fangsplitter Fangclaw Trapfang
	
	" "
}

ship_names = {
	Spite Spitter Royal Emerald Ruby Sapphire Gem Mine "Gnomekiller" Tinkerer Trap Ambusher Artificer Net Pitfall Ploy
	Bait Ruse Decoy Deceiver Lurer Hook Intrigue Plot Prank Snag Subterfuge Device
	Dragon Aakhet Alos Elkaesal Iothoral Jyntas Nimrith Tayekan Zaamalot Kobold
}

army_names = {
	"Greenscale Army" "Kobold Army" "Army of $PROVINCE$"
}

fleet_names = {
	"Greenscale Fleet" "Gem Squadron" "Storm Squadron" "Sapphire Squadron" "Ruby Squadron" "Junk Squadron" "Emerald Squadron"
}