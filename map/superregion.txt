# A region can only belong to one super region.

gerudia_superregion = { #Western Europe
	alenic_reach_region
	dalr_region
	gerudian_coast_region
}

western_cannor_superregion = { #Western Europe
	isles_of_lament_region
	dragon_coast_region
	small_country_region
	lencenor_region
	west_dameshead_region
	east_dameshead_region
	esmaria_region
	the_borders_region
	damescrown_region
	businor_region
	alenic_frontier_region
	forlorn_vale_region
}

escann_superregion = { #Western Europe
	west_castanor_region
	south_castanor_region
	inner_castanor_region
	daravans_folly_region
	dostanor_region
	ourdia_region
	deepwoods_region
}

bulwar_superregion = {
	bahar_region
	bulwar_proper_region
	harpy_hills_region
	far_bulwar_region
}

salahad_superregion = { #Western Europe
	akan_region
	gol_region
	north_salahad_region
}

north_aelantir_superregion = {
	north_aelantir_region
	trollsbay_region
	dalaire_region
	bloodgroves_region
	reapers_coast_region
}

middle_aelantir_superregion = {
	endralliande_region
	ravenous_isle_region
	ruined_isles_region
}

south_aelantir_superregion = {
	soruin_region
}

new_world_superregion ={
}



# Deprecated stuff so it doesnt crash


india_superregion = {

}

east_indies_superregion = {

}

oceania_superregion = {

}

china_superregion = {

}

europe_superregion = { #Western Europe

}

eastern_europe_superregion = {
	
}

tartary_superregion = {
	
}

far_east_superregion = {
	
}

africa_superregion = {

}

south_america_superregion = {

}

north_america_superregion = {
	
}

central_america_superregion = {

}

near_east_superregion = {
	
}

persia_superregion = {

}

# Sea super regions are used by AI to plan naval bases (it wants at least one fleet in each it has non-home presence if possible).

west_american_sea_superregion = {

}

east_american_sea_superregion = {

}

north_european_sea_superregion = {
	
}

south_european_sea_superregion = {

}

west_african_sea_superregion = {
	
}

east_african_sea_superregion = {
	
}

indian_pacific_sea_superregion = {
	
}

north_pacific_sea_superregion = {
	
}